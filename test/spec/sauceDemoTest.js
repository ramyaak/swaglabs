const loginPage = require("../pages/loginpage")
const product = require("../entities/product")


describe('SauceDemo TestCases', function () {

    this.retries(1)

    it('SauceDemo to Add a Product to Cart And Complete the Checkout process', function () {

        loginPage.login()
            .assertUserIsOnInventoryPage() //inventoryPage
            .selectProduct(product.productName)
            .assertSelectedProductIsDisplayed(product.productName) //inventoryItemPage
            .clickOnAddToCart()
            .clickOnViewCart()
            .assertProductIsAddedToCart(product.productName) //cartPage
            .clickOnCheckout()
            .fillShippingDetails() //checkoutStep1Page
            .computeTotalAndAssertPrice() //checkoutStep2Page
            .clickOnFinishButton()
            .assertCheckoutComplete() //checkoutCompletePage

            browser.pause(10000)

    })
})