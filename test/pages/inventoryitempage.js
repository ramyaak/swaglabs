const Page = require("./page")

const cartpage = require("./cartpage")

class InventoryItemPage extends Page {

    get addToCartButton() { return $('.btn_primary.btn_inventory') }

    get inventoryItemName() { return $('.inventory_details_name') }

    get clickOncart() { return $('#shopping_cart_container') }

    assertSelectedProductIsDisplayed(itemName) {
        
        const inventoryItemsName = this.inventoryItemName.getText()
        expect(inventoryItemsName).equals(itemName)
        return this
    }

    clickOnAddToCart() {

        this.click(this.addToCartButton)
        return this
    }
    clickOnViewCart() {
        
        this.click(this.clickOncart)
        return cartpage
    }


}
module.exports = new InventoryItemPage();
