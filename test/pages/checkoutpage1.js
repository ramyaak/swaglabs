const Page = require("./page")
const user = require("../entities/user")
const checkoutPage2 = require("./checkoutpage2")

class CheckoutPageOne extends Page {

    get inventoryItemList() { return $$('.inventory_item_name') }

    get checkoutButton() { return $('.btn_action.checkout_button') }
    get firstName() { return $('#first-name') }
    get lastName() { return $('#last-name') }
    get postalCode() { return $('#postal-code') }
    get continueButton() { return $('.btn_primary.cart_button') }

    fillShippingDetails() {
        
        this.type(this.firstName, user.firstName)
        this.type(this.lastName, user.lastName)
        this.type(this.postalCode, user.postalCode)
        this.click(this.continueButton)
        return checkoutPage2
    }
}
module.exports = new CheckoutPageOne();