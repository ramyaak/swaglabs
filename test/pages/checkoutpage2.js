const Page = require("./page")
const { expect } = require("chai")
const checkoutCompletePage = require("./checkoutcompletepage")

class CheckoutPageTwo extends Page {

    get inventoryItemprice() { return $('.summary_subtotal_label') }
    get tax() { return $('.summary_tax_label') }
    get summarytotal() { return $('.summary_total_label') }
    get finishButton() { return $('.btn_action.cart_button') }


    computeTotalAndAssertPrice() {

        const itemPrice = this.inventoryItemprice.getText() //Item total: $9.99
        const finalItemPrice = itemPrice.substring(13, 18)

        const tax = this.tax.getText() //Tax: $0.80
        const finalTax = tax.substring(6, 10)

        const total = parseFloat(finalItemPrice) + parseFloat(finalTax)

        const summaryTotal = this.summarytotal.getText() // $10.79
        const finalSummaryTotal = summaryTotal.substring(8, 14)

        expect(parseFloat(finalSummaryTotal)).equals(total)
        return this
    }

    clickOnFinishButton() {

        this.click(this.finishButton)
        return checkoutCompletePage
    }

}
module.exports = new CheckoutPageTwo();