const loginPage = require("./loginpage")
const page = require('./page')
const user = require("../entities/user")
const inventoryPage = require('./inventorypage')
const elementAction = require("../utilities/elementActions")
const { getViewportSize } = require("wdio-viewport-size/build/getViewportSize")

class LoginPage extends page {

    get emailAddress() { return $('#user-name') }
    get password() { return $('#password') }
    get loginButton() { return $('#login-button') }

    login() {
        
        //browser.setViewport(1024, 768)
        browser.url('/');
        //this.type(this.emailAddress, user.username)
        elementAction.doSetValue(this.emailAddress, user.username)
        //this.type(this.password, user.password)
        elementAction.doSetValue(this.password, user.password)
        //this.click(this.loginButton)
        elementAction.doClick(this.loginButton)
        return inventoryPage
    }

}
module.exports = new LoginPage();