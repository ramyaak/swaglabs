const { expect } = require("chai");

const clickTimeout = 20000;
const loadTimeout = 60000;
module.exports = class Page {
    constructor(elementRepo) {
        this.elementRepo = elementRepo;
    }
        
    click(element) {

        element.waitForClickable({ timeout: clickTimeout });
        element.click();
    }

    type(element, value) {

        element.waitForClickable({ timeout: clickTimeout });
        element.setValue(value);
    }

    waitForDisplayed(element) {

        element.waitForDisplayed({ timeout: loadTimeout });
    }

    isdisplayed(element) {

        return element.isDisplayed()
    }

}