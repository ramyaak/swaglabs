const Page = require("./page")

class CheckoutCompletePage extends Page {

    get completedLabel() { return $('.complete-header') }

    assertCheckoutComplete() {
        
        expect(this.completedLabel.getText()).equals('THANK YOU FOR YOUR ORDER')
        return this
    }
}
module.exports = new CheckoutCompletePage();