const Page = require("./page")
const checkoutpage1 = require("./checkoutpage1")

class CartPage extends Page {

    get inventoryItemList() { return $$('.inventory_item_name') }
    get checkoutButton() { return $("//a[contains(text(),'CHECKOUT')]") }

    assertProductIsAddedToCart(productName) {
        
        this.inventoryItemList.forEach(element => {
            console.log(element.getText())
            const invItemName = element.getText();
            expect(invItemName).equals(productName)
        })
        return this
    }

    clickOnCheckout() {
        this.checkoutButton.waitForClickable({ timeout: 20000 })
        this.click(this.checkoutButton)
        return checkoutpage1

    }
}
module.exports = new CartPage();