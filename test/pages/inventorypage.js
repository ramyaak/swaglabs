const Page = require("./page")
const inventoryItemPage = require("./inventoryitempage")
const elementAction = require("../utilities/elementActions")

class InventoryPage extends Page {

    get label() { return $('.product_label') }

    getSingleProduct(productName) { return $(`//div[contains(text(),'${productName}')]`) }

    assertUserIsOnInventoryPage() {

        this.waitForDisplayed(this.label)
        const labelText = elementAction.doGetText(this.label)
        expect(labelText).equals("Products")
        return this
    }

    selectProduct(productName) {
        
       // this.getSingleProduct(productName).waitForClickable({ timeout: 60000 })
       // this.getSingleProduct(productName).click()
        elementAction.doClick(this.getSingleProduct(productName))
        return inventoryItemPage
    }

}
module.exports = new InventoryPage();