const { expect } = require("chai")

class ElementActions{
    
    doClick(element){
        element.waitForDisplayed()
        element.click() // performs click operation on the element passed.
    }

    doSetValue(element, value){
        element.waitForDisplayed()
        element.setValue(value) //sets passed value to the element specified.
    }

    doGetText(element){
        element.waitForDisplayed()
        return element.getText() // returns the element text value.
    }

    doIsDisplayed(element){
        //this.waitForDisplayed(this.label)
        element.waitForDisplayed()
        return element.isDispalyed() // returns boolean val true or false.
    }

    doGetPageTitle(){
        return browser.getTitle()  // returns current browser's pageTitle.
    }
}
module.exports = new ElementActions()